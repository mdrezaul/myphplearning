<?php

echo "1.Function Array Key Exist: ";

$a= array("Volvo"=>"XC90", "BMW"=> "M5");

if(array_key_exists("Volvo", $a)) echo "Key exists<br/>";
else echo "key does not exist";

echo "<br/>";


echo "2.Function Array Merge: ";

$a1= array("Volvo","BMW");
$a2= array("Ferrari", "Benet");

$b= array_merge($a1,$a2);
echo "<pre>";
print_r($b);



echo "<br/>";

echo "3.Function Array Pad: ";

$a1= array("Volvo","BMW");


$b= array_pad($a1, 5, "Toyota");
echo "<pre>";
print_r($b);



echo "<br/>";


echo "4.Function Array Push: ";

$a1= array("Volvo","BMW");


$b=  array_push($a1, "Toyota");
echo "<pre>";
print_r($a1);



echo "<br/>";

echo "5.Function Array Pop: ";

$a=array("red","green","blue");
array_pop($a);

echo "<pre>";
print_r($a);

echo "<br/>";

echo "6.Function Array Rand: ";

$a=array("red","green","blue", "yellow", "orange");
print_r($a);

$b= array_rand($a, 3);
echo "<pre>";
echo $a[$b[0]]. "<br/>";
echo $a[$b[1]]. "<br/>";
echo $a[$b[2]]. "<br/>";
echo "<br/>";


echo "7.Function Array replace: ";

$a1= array("Volvo","BMW");
$a2= array("Ferrari", "Benet");

$b= array_replace($a1,$a2);
echo "<pre>";
print_r($b);


echo "<br/>";