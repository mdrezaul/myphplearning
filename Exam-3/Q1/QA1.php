<?php 
echo "Answer to the question no. 1. <br/>";
$str="Hello World!";

$a= explode(" ", $str);
echo "1. ";
print_r($a);
echo "<br/>";
?>

<?php 


$str1=array("Hello","World!");

$str1= implode(" ",$str1);
echo "2. ";
echo $str1;

echo "<br/>";

?>

<?php 


$str3="@^&<*>";
echo "3. ";
echo htmlentities($str3);

echo "<br/>";

?>


<?php 


$str4=" Hello ";

$str5=trim($str4, " ");
echo "4. ";
echo $str5;
echo "<br/>";

?>

<?php 


$str6=" Hello \n World ";

$str7=nl2br($str6);
echo "5. ";
echo $str7;
echo "<br/>";

?>


<?php 


$str8=" Hello World ";

$str9=str_pad($str8, 20, "+");
echo "6. ";
echo $str9;
echo "<br/>";

?>


<?php 


$str10=" Hello World";

$str11=str_repeat($str10, 10);
echo "7. ";
echo $str11;
echo "<br/>";

?>

<?php 


$str12=" Hello World";

$str13=str_replace("World", "Rezaul", $str12);
echo "8. ";
echo $str13;
echo "<br/>";

?>

<?php 


$str14=" Hello World";

$str15=str_split($str14, 6);
echo "9. ";
print_r ($str15);
echo "<br/>";

?>

<?php 


$str16=" Hello <b><i>World</b><i/>";

$str17=strip_tags($str16);
echo "10. ";
echo ($str16);
echo ($str17);
echo "<br/>";

?>



<?php 


$str18=" Hello world. Welcome to the world of PHP.";

$str19=substr_count($str18, "world");
echo "12. ";
echo ($str18);
echo "Here the word 'world' ".($str19). "times";
echo "<br/>";

?>


<?php 


$str22="hello world.";

$str23=substr_replace($str22, "Rezaul", 6 );
echo "13. ";
echo ($str23);

echo "<br/>";

?>


<?php 


$str20="hello world.";

$str21=ucfirst($str20);
echo "14. ";
echo ($str21);

echo "<br/>";

?>

<?php 


$str24="hello world.";

$str25=ucwords($str24);
echo "15. ";
echo ($str25);

echo "<br/>";

?>

